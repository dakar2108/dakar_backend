FROM node

#creo directorio de trabajo
WORKDIR /dakartechu

#añado lo que hay en el . al directorio de trabajo
ADD . /dakartechu

#Para abrir puerto
EXPOSE 3000

#en el directorio de trabajo arrancamos el node
CMD ["npm", "start"]
