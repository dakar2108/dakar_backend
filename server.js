////////////////////////////////////////////////////////////////////////////////
// --- dakar ---
// server.js
//
// Fichero sever de la aplicación dakar
// Este fichero contine la información de los métodos necesarios para el
// backend
////////////////////////////////////////////////////////////////////////////////
//
//
console.log ("------- --------------- -------");
console.log ("------------ dakar ------------");
console.log ("------ banking made easy ------");
console.log ("------ ----------------- ------");

var express =require('express') ;
var bcrypt = require('bcryptjs');
var moment = require('moment');
var jwt = require('jwt-simple');
var config = require('./config');
var util = require('./util');
var auth = require('./auth');
var mailib = require('./mailCtrl.js');
var app = express();
var sizeSaltSync =10;
var port = process.env.PORT || 3000;
var bodyParser = require ('body-parser');
app.use(bodyParser.json());
var baseMLabURL ="https://api.mlab.com/api/1/databases/apitechugargallo/collections/";
var mLabAPIKey = "apiKey=FceI0W1GziU2Z4HoA0w5Io79gO95P_IL";
var requestJson = require('request-json');
app.listen(port);

console.log("-------------------------------------------");
console.log("API dakar escuchando en el puerto: " + port);
console.log("-------------------------------------------");


//
// gargallo&mozo 01/06/2018:
//
// Funcion auxiliar chequeo letra NIF
// Devuelve true si NIF ok
// Devuelve false en cualquier otro caso
//
function checkNIF(nif) {

  console.log('[checkNIF] Inicio de la función');

  // se define la expresion regular
  var expresion_regular_dni = /^\d{8}[a-zA-Z]$/;

  // se comprueba la expresion regular
  if (expresion_regular_dni.test (nif) == true)
  {
    // expresion regular ok
    console.log('[checkNIF] Expresión regular ok');

    // se extrae numero y letra introducida
    var numero = nif.substr(0,nif.length-1);
    var letr = nif.substr(nif.length-1,1);
    console.log('[checkNIF] Número introducido:' + numero);
    console.log('[checkNIF] Letra introducida:' + letr);

    // se calcula letra esperada
    numero = numero % 23;
    var letra='TRWAGMYFPDXBNJZSQVHLCKET';
    letra=letra.substring(numero,numero+1);
    console.log('[checkNIF] Letra esperada:' + letra);

    // se comparan letra esperada y letra introducida
    if (letra!=letr.toUpperCase())
    {
      // NIF  no ok
      console.log('[checkNIF] NIF error, la letra del NIF no se corresponde');
      return false;
     }
     else
     {
       // NIF ok
       console.log('[checkNIF] NIF ok');
       return true;
     }
  }
  else
  {
    // expresion regular NO ok
    console.log('[checkNIF] NIF erroneo, formato no válido. Formato correcto: 00000000A');
    return false;
  }
}


//
// gargallo&mozo 22/05/2018:
//
  app.get("/apitechu/v2/users",
    function(req,res) {
      console.log("GET /apitechu/v2/users");
      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP creado");
      httpClient.get ("user?" + mLabAPIKey,
        function(err,resMLab,body){
          var response = !err ? body : {"msg" : "Error obteniendo usuarios"}
          res.send(response);
        }
      )
    }
  );


//
// gargallo&mozo 22/05/2018:
//
  app.get("/apitechu/v2/users/:id",
    function(req,res) {
        console.log("GET /apitechu/v2/users/:id");
        if (!auth.autentica(req,res)){
          console.log("GET /apitechu/v2/users/:id token invalido");
          var resp = {
              "msg" : "401",
              "reserror" : true
            };
            res.status(401);
            res.send(resp);

        }

        else {
          console.log("GET /apitechu/v2/users/:id token valido");

      //  var token = req.headers.token;
      //  var payload = jwt.decode(token, config.TOKEN_SECRET);
      //  if(payload.exp <= moment().unix()) {
      //    console.log("El token ha expirado")
      //      return res
      //      .status(401)
      //      .send({message: "El token ha expirado"});
      //    }
      // else{
      //   console.log("GET /apitechu/v2/users/:id token valido");
        var id = req.params.id;
        console.log("GET /apitechu/v2/users/:id-" + id+"-");
        var query = 'q={"id":' + id + '}';
        var httpClient = requestJson.createClient(baseMLabURL);
        console.log("Cliente HTTP creado");
        httpClient.get ("user?" + query  + "&" + mLabAPIKey,
          function(err,resMLab,body){
            //var response = !err ? body : {"msg" : "Error obteniendo usuario " + id}
            var response = {};
            if (err){
              response =  {"msg" : "Error obteniendo usuario " + id};
              res.status(500);
            } else {
              if (body.length > 0)
              {
                response = body;
              } else {
                response = {"msg": "Usuario no encontrado"};
                res.status (404);
              }
            }
            res.send(response);
          }
        )
      }
  }
  );


//
// gargallo&mozo 22/05/2018:
//
app.post("/apitechu/v2/login",
  function (req,res){
    console.log ("POST  /apitechu/v2/login");
    console.log("email is " + req.body.email);
    console.log("password is " + req.body.password);
    //var query = 'q={"email":"' + req.body.email + '","password":"'+ req.body.password + '"}';
    var query = 'q={"email":"' + req.body.email + '"}';
    console.log("Login_Lanzo peticion " + "user?" + query  + "&" + mLabAPIKey  );
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Login_Cliente HTTP creado");
    httpClient.get ("user?" + query  + "&" + mLabAPIKey,
      function(err,resMLab,body){
        var response = {};
        if (err){
          response =  {"msg" : "Login_Error obteniendo usuario " + req.body.email};
          res.status(500);
          res.send(response);
        } else {
          if (body.length > 0)
          {
            var salt = bcrypt.genSaltSync(sizeSaltSync);
            var idUser= body[0].id;
            var passwd= body[0].password;
            console.log(passwd);
            console.log(req.body.password);
            if (bcrypt.compareSync(req.body.password,passwd) )
            //if (true)
            {
              var putBody = '{"$set":{"logged":true}}';
              httpClient.put ("user?" + query  + "&" + mLabAPIKey,JSON.parse(putBody),
              function(err2,resMLab2,body){
                if (err){
                  response =  {"msg" : "Login_Error al hacer el put" };
                  res.status(500);
                  res.send(response);
                } else {
                    response =  {"msg" : "Login_Usuario logado correctamente, OK al hacer el put","id":idUser};
                    console.log("Login_Ha hecho el PUT");
                }
                res.send(response);
              });
            }else{
              response = {"msg": "Login_Usuario password erronea"};
              res.status (404);
              res.send(response);
            }

          } else {
            response = {"msg": "Login_Usuario no encontrado"};
            res.status (404);
            res.send(response);
          }
        }
      });
});


//
// gargallo&mozo 22/05/2018:
//
app.post("/apitechu/v2/changePasswd",
  function (req,res){
    var response = {};
    console.log ("POST  /apitechu/v2/changePasswd");
    if (!auth.autenticaconNIF(req,res)){
      console.log("GET /apitechu/v2/changePasswd token invalido");
      var resp = {
          "msg" : "401",
          "reserror" : true
        };
        res.status(401);
        res.send(resp);

    }
   // else if(payload.nif!=req.body.nif)
   // {
   //   console.log("Se esta intentado modifica un NIF distinso al logado")
   //     return res
   //     .status(401)
   //     .send({message: "Se esta intentado modifica un NIF distinso al logado"});
   // }
   else{
     console.log("GET /apitechu/v2/changePasswd token valido");
    console.log("nif is " + req.body.nif);
    console.log("password is " + req.body.password);
    console.log("password1 is " + req.body.password1);
    console.log("password2 is " + req.body.password2);
    if (req.body.password1!=req.body.password2)
    {
      response =  {"msg" : "Cambio de clave: contraseñas no son iguales.", "reserror":true};
      console.log("changePass_Error contraseñas no son iguales");
      res.status(500);
      res.send(response);
    }
    else {
          //var query = 'q={"email":"' + req.body.email + '","password":"'+ req.body.password + '"}';
          var query = 'q={"nif":"' + req.body.nif +'"}';
          console.log("changePass_peticion " + "user?" + query  + "&" + mLabAPIKey  );
          var httpClient = requestJson.createClient(baseMLabURL);
          console.log("changePass__Cliente HTTP creado");
          httpClient.get ("user?" + query  + "&" + mLabAPIKey,
          function(err,resMLab,body){
          if (err){
            response =  {"msg" : "Cambio de clave: error obteniendo usuario " + req.body.nif + " .",
                          "reserror" : true };
            console.log("changePass__Error obteniendo usuario " + req.body.nif);
            res.status(500);
            res.send(response);
          } else {
            if (body.length > 0)
            {
              var salt = bcrypt.genSaltSync(sizeSaltSync);
              var idUser= body[0].id;
              var passwd= body[0].password;
              console.log(passwd);
              console.log(req.body.password);

              if (bcrypt.compareSync(req.body.password,passwd) )
              //if (true)
              {
                console.log("id es " + idUser);
                var passEncrypted = bcrypt.hashSync(req.body.password1, salt);
                var putBody = '{"$set":{"password":"'+ passEncrypted + '"}}';
                //var putBody = '{"$set":{"email":"cgargallo@gmail.com"}}';
                httpClient.put ("user?" + query  + "&" + mLabAPIKey,JSON.parse(putBody),
                function(err2,resMLab2,body){
                  if (err){
                      response =  {"msg" : "Cambio de clave: error al hacer el put." ,
                              "reserror" : true };
                      console.log("changePass__Error al hacer el put");
                      res.status(500);
                      res.send(response);
                    } else {
                      response =  {"msg" : "Cambio de clavel: cambio de clave realizado correctamente.","id": idUser,
                                "reserror" : false };
                    console.log("changePass_hecho el PUT");
                    console.log("changePass_clave cambiada ok");
                  }
                  res.send(response);
                });
              }else  {
                response = {"msg": "Cambio de clave: password errónea.",
                              "reserror" : true};
                  console.log("changePass_ password erronea");
                res.status (404);
                res.send(response);
              }
              } else {
            response = {"msg": "Cambio de clave: usuario no encontrado.",
                          "reserror" : true};
              console.log("changePass_Usuario no encontrado");
            res.status (404);
            res.send(response);
          }
        }
      });
    }
  }
});


//
// gargallo&mozo 30/05/2018:
//
// Se define el método POST para el alta de usuario
// Se crea la cuenta automáticamente con clave obtenida en el proceso
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok
//
app.post("/apitechu/v2/addUser",
  function (req,res){
    var response = {};
    console.log ("POST  /apitechu/v2/addUser");
    console.log("nif is " + req.body.nif);
    console.log("first_name is " + req.body.first_name);
    console.log("last_name is " + req.body.last_name);
    console.log("email is " + req.body.email);
    console.log("password is " + req.body.password);
    console.log("password1 is " + req.body.password1);

    // Se comprueba que las claves sean iguales
    if (req.body.password!=req.body.password1)
    {
      // claves diferentes, fin con error

      response = {
        "msg" : "Error en Alta de Usuario: contraseñas no son iguales.",
        "reserror" : true
      };
      console.log("[addUser] Error contraseñas no son iguales");
      res.status(500);
      res.send(response);
    }
    else
    {
      // claves iguales, continua el procesamiento
      // se construye la query de consulta por nif
      var query = 'q={"nif":"' + req.body.nif + '"}';
      console.log("[addUser] Query: " + "user?" + query  + "&" + mLabAPIKey  );

      // se crea el cliente http
      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("[addUser] Cliente HTTP creado");

      // se consulta el usuario
      httpClient.get ("user?" + query + "&" + mLabAPIKey,
          function(err,resMLab,body){
          if (err)
          {
            response = {
              "msg" : "Error en Alta de Usuario: error obteniendo usuario " + req.body.nif + " .",
              "reserror" : true
            };
            console.log("[addUser] Error obteniendo usuario " + req.body.nif);
            res.status(500);
            res.send(response);

          }
          else
          {
            if (body.length > 0)
            {
              // usuario ya existe
              response = {
                "msg" : "Error en Alta de Usuario: usuario " + req.body.nif + " ya existe.",
                "reserror" : true
              };
              console.log("[addUser] El usuario ya existe");
              res.status (404);
              res.send(response);
            }
            else
            {
              // usuario no existe, se da de alta
              // se obtiene el max id
              var query = 'f={"id":1}&s={"id":-1}';
              //var query = '&s={"id":-1}';
              console.log("[addUser] sacar max id" + "user?" + query  + "&" + mLabAPIKey);

              var httpClient = requestJson.createClient(baseMLabURL);
              console.log("[addUser] Cliente HTTP creado");

              httpClient.get ("user?" + query  + "&" + mLabAPIKey,
                function(err,resMaxLab,bodyM){
                  if (err)
                  {
                    response = {
                      "msg" : "Error en Alta de Usuario: error obteniendo max id de usuario.",
                      "reserror" : true
                     };
                    console.log("[addUser] Error obteniendo usuario max ");
                    res.status(500);
                    res.send(response);
                 }
                 else
                 {
                   console.log("[addUser] Usuario consultado. Se va a comprobar el nif");
                   if (bodyM.length > 0)
                   {
                    // Consulta correcta. Se comprueba la corrección del nif
                    var nifOK = checkNIF(req.body.nif);
                    if (nifOK == false)
                    {
                      // nif incorrecto. Se informa y se termina
                      console.log("[addUser] NIF incorrecto");
                      response = {
                        "msg" : "Error en Alta de Usuario: NIF incorrecto.",
                        "reserror" : true
                      };
                      res.status(500);
                      res.send(response);
                    }
                    else
                    {
                      // nif correcto. Se da de alta el usuario
                      console.log("[addUser] NIF correcto");
                      var maxId = bodyM[0].id;
                      console.log("[addUser] El mas usuer es " + maxId);
                      console.log("[addUser] Usuario no encontrado, continua el proceso");
                      //console.log(" newUser = '{first_name:'" + req.body.first_name + ',last_name is:' + req.body.last_name + ',email:' + req.body.email + ',password:' + req.body.password + '}"';
                      var salt = bcrypt.genSaltSync(sizeSaltSync);
                      var passEncrypted = bcrypt.hashSync(req.body.password1, salt);
                      var newIdUser = parseInt(maxId+1);
                      var newUser = {
                       "id":  newIdUser,
                       "nif":  req.body.nif,
                       "first_name":  req.body.first_name,
                       "last_name": req.body.last_name,
                       "email": req.body.email ,
                       "password": passEncrypted
                      }
                      console.log("new userid is " + newIdUser);
                      httpClient.post("user?" + mLabAPIKey ,newUser,
                        function(err,resMLab,body){
                          if (err)
                          {
                            response = {
                              "msg" : "Error en Alta de Usuario: error al insertar usuario.",
                              "reserror" : true
                          };
                           console.log("[addUser] Error al insertar usuario");
                           res.status(500);
                           res.send(response);
                         }
                         else
                         {
                          // usuario dado de alta: se crea la cuenta asociada al newIdUser
                          console.log("[addUser] Se inserta la cuenta del usuario");

                          // se calcula el numero de cuenta
                          var numMili = Date.now();
                          var cadMili = numMili.toString();
                          var longCadMili = cadMili.length;
                          var restoLongCadMili = 20 - longCadMili;
                          var restoCadMili = "";
                          for (var i=1;i<=restoLongCadMili;i++){
                            // Se ejecuta N veces, con valores desde paso desde 1 hasta longResto.
                            restoCadMili = restoCadMili + "0";
                          };
                          var newIban = "ES01 " + restoCadMili + cadMili;

                          var newAccount = {
                              "user_id":  newIdUser,
                              "iban":  newIban,
                              "balance":  0,
                              "alias": "Cuenta de " + req.body.first_name
                           };

                           httpClient.post("account?" + mLabAPIKey ,newAccount,
                           function(err,resMLabMov,bodyPost){
                           if (err){
                             // error en la insercion de cuenta
                             response = {
                               "msg" : "Error en Alta de Usuario: Error en inserción cuenta asociada.",
                               "reserror" : true
                             }
                             res.status(500);
                             res.send(response);
                           }
                           else
                           {
                             response =  {
                                "msg" : "Alta de Usuario: operación correcta.",
                                "reserror" : false
                             };
                             console.log("[addUser] Usuario dado de alta con cuenta");
                             res.send(response);
                           }
                          }
                         );
                       }
                     }
                    ); // httpClient post user
                   } // if nif correcto
                  } //  if (body.length > 0)
                } //else {
              } //functoin
            ); //httpClient
          }  //else de sacamos el max
        } //else de iinea 395
      }//funcin
    );
   } //else claves iguales
 }
);


//
// gargallo&mozo 22/05/2018:
//
app.get("/apitechu/v2/users/:id/accounts",
  function(req,res) {
    console.log("GET /apitechu/v2/users/:id/accounts");
    if (!auth.autentica(req,res)){
      console.log("GET /apitechu/v2/users/:id/accounts token invalido");
      var resp = {
          "msg" : "401",
          "reserror" : true
        };
        res.status(401);
        res.send(resp);

    }
    else {
      var id = req.params.id;
      var query = 'q={"user_id":' + id + '}';
      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP creado");
      httpClient.get ("account?" + query  + "&" + mLabAPIKey,
        function(err,resMLab,body){
          var response = {};
          if (err){
            response =  {"msg" : "Error obteniendo cuentas usuario " + id};
            res.status(500);
          } else {
            if (body.length > 0)
            {
              response = body;
            } else {
              response = {"msg": "Cuentas no encontradas"};
              res.status (404);
            }
          }
          res.send(response);
        }
      )
    }
  }
);


//
// gargallo&mozo 22/05/2018:
//
// Se define el método GET  para recuperar el listado de movimientos de una cuenta
// Se devuelve una respuesta JSON
// Se devuelve un listado de movimientos
//
app.get("/apitechu/v2/accounts/:iban/",
  function(req,res) {
    if (!auth.autentica(req,res)){
      console.log("GET /apitechu/v2/accounts/:iban/ token invalido");
      var resp = {
          "msg" : "401",
          "reserror" : true
        };
        res.status(401);
        res.send(resp);

    }
    else {

      var iban = req.params.iban;
      var orden = req.headers.orden;
      var query = 'q={"iban":"' + iban + '"}&s={"date":' + orden + '}';
      console.log("GET /apitechu/v2/accounts/:iban " + query);
      var httpClient = requestJson.createClient(baseMLabURL);
      //console.log("Cliente HTTP creado: movement?" + query  + "&" + mLabAPIKey);
      console.log("Cliente HTTP creado");
      httpClient.get ("movement?" + query  + "&" + mLabAPIKey,
        function(err,resMLab,body){
          var response = {};
          if (err){
            response =  {"msg" : "Error obteniendo movimientos cuenta " + iban};
            res.status(500);
          } else {
            if (body.length > 0)
            {
              // Bucle para dar formato a la fecha
              for (var i=0;i<=body.length-1;i++){
                var f = body[i].date;
              };
              response = body;
            }
            else
            {
              response = {"msg": "No hay movimientos para iban: " + iban};
              res.status (404);
            }
          }
          res.send(response);
        }
      )
    }
  }
);


//
// gargallo&mozo 22/05/2018:
//
// Se define el método POST para logout
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok y se desloga al usuario
//
app.post("/apitechu/v2/logout",
  function(req, res) {
    console.log("POST /apitechu/v2/logout ");
    console.log("id is " + req.body.id);


    // Se crea el cliente http con la URL de base que se ha definido
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("HTTP client created")

    // Se comprueba el usuario id
    // Se crea la URL de consulta de la coleccion USER
    // Se responde
    var idUser = req.body.id;
    var query = 'q={"id":' + idUser + '}';
    var query2 = "user?" + query + "&" + mLabAPIKey;
    console.log(query);
    console.log(query2);

    // Se lanza la consulta construida en la URL
    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        var response = {};
        var userLogged = false;

        if (err) {
          response = {
            "msg" : "[500] Error logouting user",
            "reserror" : true
          }
          res.status(500);
          res.send(response);
        } else {
            if (body.length > 0) {
              response = body;
              userLogged = true;

              // Usuario existe, se marca como  no logado
              var putBody = '{"$unset" : {"logged" : ""}}';
              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(errPut, resMLabPut, bodyPut) {
                  var responsePut = {};
                  if (errPut)
                  {
                    response = {
                      "msg" : "[500] Error logouting user (PUT)",
                      "reserror" : true
                    }
                    res.status(500);
                    res.send(response);
                  }
                  else
                  {
                    response = {
                      "msg" : "Logout correct for user",
                      "reserror" : false
                    };
                    res.send(response);
                  }
                } // function
              );
            }
            else {
              // Usuario no existe, se devuelve error
              response = {
                "msg" : "[404] User not found",
                "reserror" : true
              }
              res.status(404);
              res.send(response);
            }
        }
      }
    );
  }
);


//
// gargallo&mozo 22/05/2018:
//
// Se define el método POST para la modificación de usuario
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok
//
app.post("/apitechu/v2/modifyUser",
  function(req, res) {
    console.log("POST /apitechu/v2/modifyUser");
    console.log("id is " + req.body.id);
    console.log("new_first_name is " + req.body.first_name);
    console.log("new_last_name is " + req.body.last_name);
    console.log("new_email is " + req.body.email);

    // Se crea el cliente http con la URL de base que se ha definido
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("HTTP client created")

    // Se comprueba el usuario id
    // Se crea la URL de consulta de la coleccion user
    // Se responde
    var idUser = req.body.id;
    var query = 'q={"id":' + idUser + '}';
    console.log(query);

    // Se lanza la consulta construida en la URL
    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        var response = {};

        if (err) {
          response = {
            "msg" : "Modificación de Usuario: error chequeando existencia usuario.",
            "reserror" : true
          }
          res.status(500);
          res.send(response);
        } else {
            if (body.length > 0) {
              response = body;

              // Usuario existe, se modifican los campos del body
              var putBody = '{"$set":{"first_name":"'+ req.body.first_name + '"'
                  +',"last_name":"'+ req.body.last_name + '"'
                  +',"email":"'+ req.body.email + '"}}';

              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(errPut, resMLabPut, bodyPut) {
                  var responsePut = {};
                  if (errPut)
                  {
                    response = {
                      "msg" : "Modificación de Usuario: error actualizando valores.",
                      "reserror" : true
                    }
                    res.status(500);
                    res.send(response);
                  }
                  else
                  {
                    response = {
                      "msg" : "Modificación de Usuario: operación correcta.",
                      "reserror" : false
                    };
                    res.send(response);
                  }
                } // function put
              );
            }
            else {
              // Usuario no existe, se devuelve error
              response = {
                "msg" : "Modificación de Usuario: usuario no existe.",
                "reserror" : true
              }
              res.status(404);
              res.send(response);
            }
        }
      }
    );
  } // function post manejadora
); // post modifyUser


//
// gargallo&mozo 01/06/2018:
//
// Se define el método POST para la transferencia con clave enviada al correo
// Se asume que el procesamiento de la transferencia en la cuenta destino no
//   se realiza: no habrá modificación del saldo en cuenta destino
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok
//
app.post("/apitechu/v2/makeTransfer0",
  function(req, res) {
    console.log("[makeTransfer0] POST /apitechu/v2/makeTransfer0");

    if (!auth.autentica(req,res)){
      console.log("GET /apitechu/v2/makeTransfer0 token invalido");
      var resp = {
          "msg" : "401",
          "reserror" : true
        };
        res.status(401);
        res.send(resp);

    }

    else {

      // Se crea el cliente http con la URL de base que se ha definido
      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("[makeTransfer0] HTTP client created")

      // Se comprueba la existencia de la cuenta origen
      var iban = req.body.iban;
      var query = 'q={"iban":"' + iban + '"}';
      console.log(query);

      // Se lanza la consulta construida en la URL
      httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body){
          var response = {};

          if (err) {
            // Error en la consulta de la cuenta
            response = {
              "msg" : "Transferencia: error comprobando cuenta.",
              "reserror" : true
            }
            res.status(500);
            res.send(response);

          } else {

              if (body.length > 0) {
                // La cuenta existe, se chequea el saldo
                // En body[0].balance se dispone del saldo
                // En req.body.amount se dispone del importe
                var balance = body[0].balance;
                // console.log("req.body.amount: "+ req.body.amount);
                var amount = parseFloat(req.body.amount);
                // console.log("amount: "+ amount);
                var userEmail = req.body.email;
                var divisa = req.body.currency;
                var amountOrg = req.body.amountOrg;

                if (balance >= amount)
                {
                  // Saldo suficiente: se actualiza saldo y se inserta movimiento
                  // mando email, con clave entera aleatoria
                  var min = 0;
                  var max = 10000;
                  var otpS = Math.floor(Math.random() * (max - min)) + min;
                  var otpL = otpS.toString();
                  var otp = otpL.substr(0, 5);
                  var cuerpo = 'Su código de verificación para confirmar su transferencia de importe ' + util.numberFormat(amount,2) + " EUR ";
                  if (divisa!="EUR")
                  {  cuerpo = cuerpo + "(" + util.numberFormat(amountOrg,2) + " " + divisa + ")";
                  }
                  cuerpo = cuerpo +  " es: " + otp;



                  var mailOptions = {
                    from: 'DAKAR BANK',
                    to: userEmail,
                    subject: 'OTP de confirmación',
                    text: cuerpo
                  };
                  if (mailib.sendEmail (req,res,mailOptions)){
                    console.log("mail enviado");
                    res.status (200);

                    response = {
                      "msg" : "[makeTransfer0] Mail sent OK",
                      "otp": otp,
                      "exp": moment().add(1, "minutes").unix()
                    };
                    res.send(response);
                  }
                  else  {
                    response = {
                        "msg" : "Transferencia: Error al enviar el mail de clave de confirmación.",
                        "reserror" : true
                      }
                      res.status(404);
                      res.send(response);
                  }
                }
                else {
                  // Saldo insuficiente: la transferencia no se realiza
                  response = {
                      "msg" : "Transferencia: saldo insuficiente.",
                      "reserror" : true
                    }
                    res.status(404);
                    res.send(response);
                }
              }
              else {
                // La cuenta no existe, se devuelve error
                response = {
                  "msg" : "Transferencia: cuenta no encontrada.",
                  "reserror" : true
                }
                res.status(404);
                res.send(response);
              }
          }
        }
      );
    }
  } // function post makeTransfer0
); // post makeTransfer0


//
// gargallo&mozo 22/05/2018:
//
// Se define el método POST para la transferencia
// Se asume que el procesamiento de la transferencia en la cuenta destino no
//   se realiza: no habrá modificación del saldo en cuenta destino
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok
//
app.post("/apitechu/v2/makeTransfer",
  function(req, res) {
    console.log("[makeTransfer] POST /apitechu/v2/makeTransfer");
    if (req.body.exp <= moment().unix())
    {
        console.log("/apitechu/v2/makeTransfer La OTP ha expirado");
        response = {
          "msg" : "La OTP ha expirado",
          "reserror" : true
        }
        res.status(500);
        res.send(response);
    }
    else   {


      // Se crea el cliente http con la URL de base que se ha definido
      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("[makeTransfer] HTTP client created")

      // Se comprueba la existencia de la cuenta origen
      var iban = req.body.iban;
      var query = 'q={"iban":"' + iban + '"}';
      console.log(query);

      // Se lanza la consulta construida en la URL
      httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body){
          var response = {};

          if (err) {
            // Error en la consulta de la cuenta
            response = {
              "msg" : "Transferencia: error comprobando cuenta.",
              "reserror" : true
            }
            res.status(500);
            res.send(response);

          } else {

              if (body.length > 0) {
                // La cuenta existe, se chequea el saldo
                // En body[0].balance se dispone del saldo
                // En req.body.amount se dispone del importe
                var balance = body[0].balance;
                var amount = parseFloat(req.body.amount);

                if (balance >= amount)
                {
                  // Saldo suficiente: se actualiza saldo y se inserta movimiento
                  var newBalance = balance - amount;
                  var putBody = '{"$set":{"balance":'+  newBalance + '}}';
                  console.log(putBody);
                  httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                    function(errPut, resMLabPut, bodyPut) {
                      var responsePut = {};
                      if (errPut)
                      {
                        // error en Put: no se actualiza saldo ni se inserta movimiento
                        response = {
                          "msg" : "Transferencia: error actualizando saldo.",
                          "reserror" : true
                        }
                        res.status(500);
                        res.send(response);
                      }
                      else
                      {
                        // Saldo actualizado. Se inserta movimiento
                        var fecha = new Date();
                        var orig_currency="";
                        var orig_amount ="";
                        if (req.body.orig_currency!="EUR")
                        {
                           orig_currency=req.body.orig_currency;
                           orig_amount =req.body.orig_amount;
                        }
                        var newMovement = {
                          "iban": req.body.iban,
                          "other_iban":  req.body.other_iban,
                          "other_name":  req.body.other_name,
                          "type": "cargo",
                          "sub_type": "Transferencia" ,
                          "concept": req.body.concept,
                          "currency": "EUR",
                          "orig_currency":orig_currency,
                          "orig_amount":orig_amount,
                          "date": fecha,
                          "amount": amount
                        };

                        // console.log ("orig_amount " + orig_amount);
                        // console.log ("orig_currency " + orig_currency);
                        // console.log ("other_name " + req.body.other_name);

                        httpClient.post("movement?" + mLabAPIKey ,newMovement,
                        function(err,resMLabMov,bodyPost){
                        if (err){
                          // error en la insercion de movimiento
                          response = {
                            "msg" : "Transferencia: error insertando movimiento.",
                            "reserror" : true
                          }
                          res.status(500);
                          res.send(response);
                        } else
                        {
                          // Movimiento insertado correctamente
                          response = {
                            "msg" : "Transferencia: operación realizada correctamente.",
                            "reserror" : false
                          };
                          res.send(response);                      }
                      }
                    ); // httpClient
                      }
                    } // function put
                  );
                }
                else {
                  // Saldo insuficiente: la transferencia no se realiza
                  response = {
                      "msg" : "Transferencia: saldo insuficiente.",
                      "reserror" : true
                    }
                    res.status(404);
                    res.send(response);
                }
              }
              else {
                // La cuenta no existe, se devuelve error
                response = {
                  "msg" : "Transferencia: cuenta no encontrada.",
                  "reserror" : true
                }
                res.status(404);
                res.send(response);
              }
          }
        }
      );
    }
  } // function post makeTransfer
); // post makeTransfer


//
// gargallo&mozo 22/05/2018:
//
// Se define el método POST para el ingreso
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok
//
app.post("/apitechu/v2/makeIncome",
  function(req, res) {
    console.log("[makeIncome] POST /apitechu/v2/makeIncome");

    if (!auth.autentica(req,res)){
      console.log("GET /apitechu/v2/makeIncome token invalido");
      var resp = {
          "msg" : "401",
          "reserror" : true
        };
        res.status(401);
        res.send(resp);

    }

    else {

      // Se crea el cliente http con la URL de base que se ha definido
      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("[makeIncome] HTTP client created")

      // Se comprueba la existencia de la cuenta origen
      var iban = req.body.iban;
      var query = 'q={"iban":"' + iban + '"}';
      console.log(query);

      // Se lanza la consulta construida en la URL
      httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body){
          var response = {};

          if (err) {
            // Error en la consulta de la cuenta
            response = {
              "msg" : "Ingreso: error comprobando cuenta.",
              "reserror" : true
            }
            res.status(500);
            res.send(response);

          } else {

              if (body.length > 0) {
                // La cuenta existe, se chequea el saldo
                // En body[0].balance se dispone del saldo
                // En req.body.amount se dispone del importe
                var balance = body[0].balance;
                var amount = parseFloat(req.body.amount);
                console.log("[makeIncome] Balance: " + balance);
                console.log("[makeIncome] Amount: " + amount);

                  // Saldo suficiente: se actualiza saldo y se inserta movimiento
                  var newBalance = balance + amount;
                  var putBody = '{"$set":{"balance":'+  newBalance + '}}';
                  console.log(putBody);
                  httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                    function(errPut, resMLabPut, bodyPut) {
                      var responsePut = {};
                      if (errPut)
                      {
                        // error en Put: no se actualiza saldo ni se inserta movimiento
                        response = {
                          "msg" : "Ingreso: error actualizando saldo.",
                          "reserror" : true
                        }
                        res.status(500);
                        res.send(response);
                      }
                      else
                      {
                        // Saldo actualizado. Se inserta movimiento
                        var fecha = new Date();
                        var newMovement = {
                          "iban": req.body.iban,
                          "type": "abono",
                          "sub_type": "Ingreso" ,
                          "concept": req.body.concept,
                          "currency": "EUR",
                          "date": fecha,
                          "amount": amount
                        };

                        httpClient.post("movement?" + mLabAPIKey ,newMovement,
                        function(err,resMLabMov,bodyPost){
                        if (err){
                          // error en la insercion de movimiento
                          response = {
                            "msg" : "Ingreso: error insertando movimiento.",
                            "reserror" : true
                          }
                          res.status(500);
                          res.send(response);
                        } else
                        {
                          // Movimiento insertado correctamente
                          response = {
                            "msg" : "Ingreso: operación realizada correctemente.",
                            "reserror" : false
                          };
                          res.send(response);                      }
                         }
                       ); // httpClient
                      }
                    } // function put
                  );
              }
              else {
                // La cuenta no existe, se devuelve error
                response = {
                  "msg" : "Ingreso: cuenta no encontrada.",
                  "reserror" : true
                }
                res.status(404);
                res.send(response);
              }
          }
        }
      );
    }
  } // function post makeIncome
); // post makeIncome


//
// gargallo&mozo 28/05/2018:
//
// Se define el método POST para el reintegro
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok
//
app.post("/apitechu/v2/makeRefund",
  function(req, res) {
    console.log("[makeRefund] POST /apitechu/v2/makeRefund");
    if (!auth.autentica(req,res)){
      console.log("GET /apitechu/v2/makeRefund token invalido");
      var resp = {
          "msg" : "401",
          "reserror" : true
        };
        res.status(401);
        res.send(resp);

    }

    else {
      // Se crea el cliente http con la URL de base que se ha definido
      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("[makeRefund] HTTP client created")

      // Se comprueba la existencia de la cuenta origen
      var iban = req.body.iban;
      var query = 'q={"iban":"' + iban + '"}';
      console.log(query);

      // Se lanza la consulta construida en la URL
      httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body){
          var response = {};

          if (err) {
            // Error en la consulta de la cuenta
            response = {
              "msg" : "Retirada: error comprobando cuenta.",
              "reserror" : true
            }
            res.status(500);
            res.send(response);

          } else {

              if (body.length > 0) {
                // La cuenta existe, se chequea el saldo
                // En body[0].balance se dispone del saldo
                // En req.body.amount se dispone del importe
                var balance = body[0].balance;
                var amount = parseFloat(req.body.amount);
                console.log("[makeRefund] Balance: " + balance);
                console.log("[makeRefund] Amount: " + amount);
                if (balance >= amount)
                {
                  // Saldo suficiente: se actualiza saldo y se inserta movimiento
                  var newBalance = balance - amount;
                  var putBody = '{"$set":{"balance":'+  newBalance + '}}';
                  console.log(putBody);
                  httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                    function(errPut, resMLabPut, bodyPut) {
                      var responsePut = {};
                      if (errPut)
                      {
                        // error en Put: no se actualiza saldo ni se inserta movimiento
                        response = {
                          "msg" : "Retirada: error actualizando saldo.",
                          "reserror" : true
                        }
                        res.status(500);
                        res.send(response);
                      }
                      else
                      {
                        // Saldo actualizado. Se inserta movimiento
                        var fecha = new Date();
                        var newMovement = {
                          "iban": req.body.iban,
                          "type": "cargo",
                          "sub_type": "Reintegro" ,
                          "concept": req.body.concept,
                          "currency": "EUR",
                          "date": fecha,
                          "amount": amount
                        };

                        httpClient.post("movement?" + mLabAPIKey ,newMovement,
                        function(err,resMLabMov,bodyPost){
                        if (err){
                          // error en la insercion de movimiento
                          response = {
                            "msg" : "Retirada: error insertando movimiento.",
                            "reserror" : true
                          }
                          res.status(500);
                          res.send(response);
                        } else
                        {
                          // Movimiento insertado correctamente
                          response = {
                            "msg" : "Retirada: operación realizada correctamente.",
                            "reserror" : false
                          };
                          res.send(response);                      }
                      }
                    ); // httpClient
                      }
                    } // function put
                  );
                }
                else {
                  // Saldo insuficiente
                  response = {
                    "msg" : "Retirada: saldo insuficiente.",
                    "reserror" : true
                  }
                  res.status(404);
                  res.send(response);
                }
              }
              else {
                // La cuenta no existe, se devuelve error
                response = {
                  "msg" : "Retirada: cuenta no encontrada",
                  "reserror" : true
                }
                res.status(404);
                res.send(response);
              }
          }
        }
      );
    }
  } // function post makeRefund
); // post makeRefund


//
// gargallo&mozo 22/05/2018:
//
// Se define el método POST para la modificación de cuenta
// Esta funcion solo modifica el alias de la cuenta. El resto de atributos
// no son modificables
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok
//
app.post("/apitechu/v2/modifyAccount",
  function(req, res) {
    console.log("[modifyAccount] POST /apitechu/v2/modifyAccount");
    console.log("iban is " + req.body.iban);
    console.log("new_alias is " + req.body.alias);

    // Se crea el cliente http con la URL de base que se ha definido
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("[modifyAccount] HTTP client created")

    // Se comprueba el usuario id
    // Se crea la URL de consulta de la coleccion user
    // Se responde
    var iban = req.body.iban;
    var query = 'q={"iban":"' + iban + '"}';
    console.log(query);

    // Se lanza la consulta construida en la URL
    httpClient.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        var response = {};

        if (err) {
          response = {
            "msg" : "Modificar cuenta: error comprobando existencia de cuenta.",
            "reserror" : true
          }
          res.status(500);
          res.send(response);
        } else {
            if (body.length > 0) {
              response = body;

              // Cuenta existe, se modifican los campos del body
              var putBody = '{"$set":{"alias":"'+ req.body.alias + '"}}';

              httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(errPut, resMLabPut, bodyPut) {
                  var responsePut = {};
                  if (errPut)
                  {
                    response = {
                      "msg" : "Modificar cuenta: error estableciendo alias para la cuenta.",
                      "reserror" : true
                    }
                    res.status(500);
                    res.send(response);
                  }
                  else
                  {
                    response = {
                      "msg" : "Modificar cuenta: alias modificado correctamente.",
                      "reserror" : false
                    };
                    res.send(response);
                  }
                } // function put
              );
            }
            else {
              // Cuenta no existe, se devuelve error
              response = {
                "msg" : "Modificar cuenta: cuenta no existe.",
                "reserror" : true
              }
              res.status(404);
              res.send(response);
            }
        }
      }
    );
  } // function post manejadora
); // post modifyAccount


//
// gargallo&mozo 25/05/2018:
//
// Se define el método POST para el login por NIF
// Se asume que solo existe un usuario por NIF
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok
//
app.post("/apitechu/v2/loginNIF",
  function (req,res){
    console.log ("[loginNIF] POST /apitechu/v2/loginNIF");
    console.log("[loginNIF] nif is " + req.body.nif);
    console.log("[loginNIF] password is " + req.body.password);

    // Se crea el cliente http con la URL de base que se ha definido
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("[loginNIF] HTTP client created")

    // Se comprueba el usuario nif
    // Se crea la URL de consulta de la coleccion user
    //var query = 'q={"nif":"' + req.body.nif + '","password":"'+ req.body.password + '"}';
    var query = 'q={"nif":"' + req.body.nif + '"}';
    console.log(query);

    // Se lanza la consulta construida en la URL
    httpClient.get ("user?" + query  + "&" + mLabAPIKey,
      function(err,resMLab,body){
        var response = {};

        if (err){
          response = {
            "msg" : "Error comprobando usuario por NIF " + req.body.nif,
            "reserror" : true
          };
          res.status(500);
          res.send(response);
        } else {
          if (body.length > 0)
          {
            response = body;
            var nifUser= body[0].nif;
            var idUser= body[0].id;
            var salt = bcrypt.genSaltSync(sizeSaltSync);
            var passwd= body[0].password;
            var email= body[0].email;
            console.log(passwd);
            console.log(req.body.password);
            if (bcrypt.compareSync(req.body.password,passwd) )
            {
              // Usuario existe, se marca como logado
              var putBody = '{"$set":{"logged":true}}';

              console.log("USUARIO ENCONTRADO");
              console.log("[putBody] " + putBody);
              console.log("[put] " + "user?" + query + "&" + mLabAPIKey);

              httpClient.put ("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function(err2,resMLab2,bodyPut){
                if (err){
                  response = {
                    "msg" : "Error actualizando campo logged.",
                    "reserror" : true
                  };
                  res.status(500);
                  res.send(response);
                } else {
                    /*response = {
                      "msg" : "[loginNIF] User logged OK",
                      "nif": nifUser,
                      "id" : idUser,
                      "reserror" : false
                    };
                    res.send(response);*/
                    var timestamp= new Date();
                    var user = {
                        "id":  idUser,
                        "nif":  nifUser,
                        iat: moment().unix(),
                        exp: moment().add(1, "hours").unix(),
                      };
                    var mitoken = jwt.encode(user, config.TOKEN_SECRET);


                    res.status (200);
                    response = {
                      "msg" : "Usuario logado OK",
                      "nif": nifUser,
                      "id" : idUser,
                      "email": email,
                      "token" : mitoken
                    };
                    res.send(response);
                  }
              });
          }else{
            response = {
              "msg": "Password incorrecta",
              "reserror" : true
            };
            res.status (404);
            res.send(response);
          }
          } else {
            response = {
              "msg": "Usuario no encontrado",
              "reserror" : true
            };
            res.status (404);
            res.send(response);
          }
        }
      });
    }// function post manejadora
);// post loginNIF


//
// gargallo&mozo 22/05/2018:
//
// Se define el método POST para el traspaso
// PRE: Se debe llamar con dos cuentas del mismo usuario
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok
//
//
app.post("/apitechu/v2/makeInternalTransfer",
  function(req, res) {
    console.log("[makeInternalTransfer] POST /apitechu/v2/makeInternalTransfer");

    if (!auth.autentica(req,res)){
      console.log("GET /apitechu/v2/makeInternalTransfer token invalido");
      var resp = {
          "msg" : "401",
          "reserror" : true
        };
        res.status(401);
        res.send(resp);

    }

    else {

      // Se crea el cliente http con la URL de base que se ha definido
      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("[makeInternalTransfer] HTTP client created")

      // Se almacenan las variables del body
      var iban = req.body.iban;
      var other_iban = req.body.other_iban;
      var concept = req.body.concept;
      var currency = "EUR";
      var amount = parseFloat(req.body.amount);

      // Se comprueba la existencia de la cuenta origen
      var queryOrigin = 'q={"iban":"' + iban + '"}';
      console.log(queryOrigin);

      // Se lanza la consulta construida en la URL
      httpClient.get("account?" + queryOrigin + "&" + mLabAPIKey,
        function(err, resMLab, body){

          var response = {};
          if (err) {
            // Error en la consulta de la cuenta origen
            response = {
              "msg" : "Traspaso: error comprobando cuenta origen.",
              "reserror" : true
            }
            res.status(500);
            res.send(response);
          } else {
              // Exito en la consulta de la cuenta origen
              if (body.length > 0) {
                // La cuenta origen existe, se almacena el saldo y se chequea
                // la existencia de la cuenta destino
                // En body[0].balance se dispone del saldo
                var balanceOrigin = parseFloat(body[0].balance);

                // Se comprueba la existencia de la cuenta destino
                var queryDest = 'q={"iban":"' + other_iban + '"}';
                console.log(queryDest);

                // Se lanza la consulta construida en la URL
                httpClient.get("account?" + queryDest + "&" + mLabAPIKey,
                  function(err, resMLab, body){
                    var response = {};
                    if (err) {
                      // Error en la consulta de la cuenta destino
                      response = {
                        "msg" : "Traspaso: error comprobando cuenta destino.",
                        "reserror" : true
                      }
                      res.status(500);
                      res.send(response);
                    } else {
                        if (body.length > 0){
                          // La cuenta destino existe, se almacena el saldo
                          // En body[0].balance se dispone del saldo
                          var balanceDestination = parseFloat(body[0].balance);

                          // Se comprueba el saldo de la cuenta origen
                          if (balanceOrigin >= amount ) {
                            // Saldo suficiente: actualizar saldos e insertar movimientos
                            console.log("[makeInternalTransfer] Saldo suficiente");

                            // Se inserta movimiento de cargo en origen
                            var fecha = new Date();
                            var newMovementCargo = {
                              "iban": iban,
                              "other_iban" : other_iban,
                              "type": "cargo",
                              "sub_type": "Traspaso emitido" ,
                              "concept": concept,
                              "currency": currency,
                              "date": fecha,
                              "amount": amount
                            };
                            httpClient.post("movement?" + mLabAPIKey ,newMovementCargo,
                            function(err,resMLabMov,bodyPost){
                            if (err){
                              // error en la insercion de movimiento
                              response = {
                                "msg" : "Traspaso: error inserción movimiento cuenta origen.",
                                "reserror" : true
                              }
                              res.status(500);
                              res.send(response);
                            } else
                            {
                              // Movimiento cargo insertado correctamente
                              // Se inserta movimiento abono
                              var newMovementAbono = {
                                "iban": other_iban,
                                "other_iban" : iban,
                                "type": "abono",
                                "sub_type": "Traspaso recibido" ,
                                "concept": concept,
                                "currency": currency,
                                "date": fecha,
                                "amount": amount
                              };

                              httpClient.post("movement?" + mLabAPIKey ,newMovementAbono,
                              function(err,resMLabMov,bodyPost){
                              if (err){
                                // error en la insercion de movimiento
                                response = {
                                  "msg" : "Traspaso: error inserción movimiento cuenta destino.",
                                  "reserror" : true
                                }
                                res.status(500);
                                res.send(response);
                              } else
                              {
                                // Movimiento insertado correctamente
                                // Se actualizan saldos
                                var newBalanceOrigin = balanceOrigin - amount;
                                var putBody = '{"$set":{"balance":'+  newBalanceOrigin + '}}';
                                console.log(putBody);
                                httpClient.put("account?" + queryOrigin + "&" + mLabAPIKey, JSON.parse(putBody),
                                  function(errPut, resMLabPut, bodyPut) {
                                    var responsePut = {};
                                    if (errPut)
                                    {
                                      // error en Put: no se actualiza saldo ni se inserta movimiento
                                      response = {
                                        "msg" : "Traspaso: error actualización saldo cuenta origen.",
                                        "reserror" : true
                                      }
                                      res.status(500);
                                      res.send(response);
                                    }
                                    else
                                    {
                                      console.log("[makeInternalTransfer] OK saldo origen");
                                      // Actualizado saldo cuenta origen
                                      // Se actualiza saldo cuenta destino
                                      var newBalanceDest = balanceDestination + amount;
                                      var putBody = '{"$set":{"balance":'+  newBalanceDest + '}}';
                                      console.log(putBody);
                                      httpClient.put("account?" + queryDest + "&" + mLabAPIKey, JSON.parse(putBody),
                                        function(errPut, resMLabPut, bodyPut) {
                                          var responsePut = {};
                                          if (errPut)
                                          {
                                            // error en Put: no se actualiza saldo ni se inserta movimiento
                                            response = {
                                              "msg" : "Traspaso: error actualización saldo cuenta destino.",
                                              "reserror" : true
                                            }
                                            res.status(500);
                                            res.send(response);
                                          }
                                          else
                                          {
                                            console.log("[makeInternalTransfer] OK saldo destino");
                                            response = {
                                              "msg" : "Traspaso: traspaso realizado correctamente.",
                                              "reserror" : false
                                            };
                                            res.send(response);
                                          }
                      				          }
                      				        );
                                    }
                                }
                              );
                             }
                            }
                            ); // httpClient insert movimiento abono
                            }
                          }
                        ); // httpClient insert movimiento cargo
                      }
                          else {
                            // Saldo insuficiente: la transferencia no se realiza
                            console.log("[makeInternalTransfer] Saldo insuficiente");
                            response = {
                                 "msg" : "Traspaso: saldo insuficiente en cuenta origen.",
                                 "reserror" : true
                             }
                             res.status(404);
                             res.send(response);
                          }
                        }
                        else {
                          // La cuenta destino no existe, se devuelve error
                          response = {
                            "msg" : "Traspaso: cuenta destino inexistente.",
                            "reserror" : true
                          }
                          res.status(404);
                          res.send(response);
                        }
                    }
                  }
                );
              }
              else {
                // La cuenta origen no existe, se devuelve error
                response = {
                  "msg" : "Traspaso: cuenta origen inexistente.",
                  "reserror" : true
                }
                res.status(404);
                res.send(response);
              }
          }
        }
      );
    }
  } // function post makeInternalTransfer
); // post makeInternalTransfer


//
// gargallo&mozo 27/05/2018:
//
// Se define el método GET  para recuperar el listado de movimientos de una cuenta
// filtrando por importe
// Se devuelve una respuesta JSON
// Se devuelve un listado de movimientos
//
app.get("/apitechu/v2/accounts/amountFilter/:iban/:low/:high",
  function(req,res) {
      console.log("[amountFilter] GET /apitechu/v2/accounts/amountFilter/:iban/:low/:high");
      if (!auth.autentica(req,res)){
        console.log("GET amountFilter] GET /apitechu/v2/accounts/amountFilter/:iban/:low/:high token invalido");
        var resp = {
            "msg" : "401",
            "reserror" : true
          };
          res.status(401);
          res.send(resp);
      }
      else {
        // Se crea el cliente http con la URL de base que se ha definido
        var httpClient = requestJson.createClient(baseMLabURL);
        console.log("[amountFilter] HTTP client created")

        // se almacenan los parametros de entrada
        var iban = req.params.iban;
        // var lowAmount = req.headers.lowA;
        // var highAmount = req.headers.highA;
        var lowAmount = req.params.low;
        var highAmount = req.params.high;

        var orden=req.headers.orden;
        console.log("[amountFilter] iban:" + iban);
        console.log("[amountFilter] low:" + lowAmount);
        console.log("[amountFilter] high:" + highAmount);

        // Se consultan los movimientos de la cuenta, filtrando por importe
        var query = 'q={"$and":[{"iban":"' + iban + '"},{"$and":[{"amount":{$lte:' + highAmount + '}},{"amount":{$gte:' + lowAmount + '}}]}]}&s={"date":'+ orden +'}';
        console.log(query);

        httpClient.get ("movement?" + query + "&" + mLabAPIKey,
          function(err,resMLab,body){
            var response = {};
            if (err){
              response =  {
                "msg" : "[amountFilter] Error obteniendo movimientos cuenta " + iban,
                "reserror" : true
              };
              res.status(500);
              res.send(response);
            } else
            {
              if (body.length > 0)
              {
                response = body;
                res.send(response);
              } else {
                response = {
                  "msg": "[amountFilter] No hay movimientos para iban: " + iban,
                  "reserror" : true
                }
                res.status (404);
                res.send(response);
              }
            }
          } // funcion manejadora get movement
        ); // get movement
    }
  } // funcion manejadora principal
); // get amountFilter


//
// gargallo&mozo 29/05/2018:
//
// Se define el método GET  para recuperar el listado de movimientos de una cuenta
// filtrando por fecha
// Se devuelve una respuesta JSON
// Se devuelve un listado de movimientos
//
app.get("/apitechu/v2/accounts/dateFilter/:iban/:low/:high",
  function(req,res) {
    console.log("[dateFilter] GET /apitechu/v2/accounts/dateFilter/:iban/:low/:high");
    if (!auth.autentica(req,res)){
      console.log("[dateFilter] GET /apitechu/v2/accounts/dateFilter/:iban/:low/:high token invalido");
      var resp = {
          "msg" : "401",
          "reserror" : true
        };
        res.status(401);
        res.send(resp);

    }
    else {
      // Se crea el cliente http con la URL de base que se ha definido
      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("[dateFilter] HTTP client created")

      // se almacenan los parametros de entrada
      var iban = req.params.iban;
      var lowDate = req.params.low;
      var highDate = req.params.high;
      console.log("[dateFilter] iban:" + iban);
      console.log("[dateFilter] low:" + lowDate);
      console.log("[dateFilter] high:" + highDate);

      // Se consultan los movimientos de la cuenta, filtrando por fecha
      var query = 'q={"$and":[{"iban":"' + iban + '"},{"$and":[{"date":{$lte:"' + highDate + '"}},{"date":{$gte:"' + lowDate + '"}}]}]}&s={"date":-1}';
      console.log(query);

      httpClient.get ("movement?" + query + "&" + mLabAPIKey,
        function(err,resMLab,body){
          var response = {};
          if (err){
            response =  {
              "msg" : "[dateFilter] Error obteniendo movimientos cuenta " + iban,
              "reserror" : true
            };
            res.status(500);
            res.send(response);
          } else
          {
            if (body.length > 0)
            {
              response = body;
              res.send(response);
            } else {
              response = {
                "msg": "[dateFilter] No hay movimientos para iban: " + iban,
                "reserror" : true
              }
              res.status (404);
              res.send(response);
            }
          }
        } // funcion manejadora get movement
      ); // get movement
    }
  } // funcion manejadora principal
); // get dateFilter


//
// gargallo&mozo 01/06/2018:
//
// Se define el método GET  para recuperar el saldo de una cuenta
// Se devuelve una respuesta JSON
// Se devuelve un saldo
//
app.get("/apitechu/v2/accounts/actualBalance/:iban",
  function(req,res) {
    console.log("[actualBalance] GET /apitechu/v2/accounts/actualBalance/:iban");

    // Se crea el cliente http con la URL de base que se ha definido
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("[actualBalance] HTTP client created")

    // se almacenan los parametros de entrada
    var iban = req.params.iban;
    console.log("[actualBalance] iban:" + iban);

    // Se consulta el saldo de la cuenta
    var query = 'q={"iban":"' + iban + '"}';
    console.log(query);

    httpClient.get ("account?" + query + "&" + mLabAPIKey,
      function(err,resMLab,body){
        var response = {};
        if (err){
          response =  {
            "msg" : "[actualBalance] Error obteniendo cuenta " + iban,
            "reserror" : true
          };
          res.status(500);
          res.send(response);
        } else
        {
          if (body.length > 0)
          {
            console.log("[actualBalance] Cuenta encontrada: " + iban);
            console.log("[actualBalance] Saldo: " + body[0].balance);
            res.status (200);
            response = {
              "msg" : "[actualBalance] OK",
              "saldo": body[0].balance
            };
            res.send(response);


          } else {
            response = {
              "msg": "[actualBalance] No exite la cuenta: " + iban,
              "reserror" : true
            }
            res.status (404);
            res.send(response);
          }
        }
      } // funcion manejadora get account
    ); // get account
  } // funcion manejadora principal
); // get actualBalance


//
// gargallo&mozo 01/06/2018:
//
// Se define el método POST para añadir cuenta
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok
//
app.post("/apitechu/v2/addAccount",
  function(req, res) {
    console.log("[addAccount] POST /apitechu/v2/addAccount");

    // Se crea el cliente http con la URL de base que se ha definido
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("[addAccount] HTTP client created")

    // Se almacenan parametros del body
    var idUser = req.body.id;
    var alias = req.body.alias;
    console.log("[addAccount] id: " + idUser);
    console.log("[addAccount] alias: " + alias);

    // Se comprueba la existencia del usario peticionario
    var query = 'q={"user_id":' + idUser + '}';
    console.log(query);

    // Se lanza la consulta construida en la URL
    httpClient.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        var response = {};

        if (err)
        {
          // Error en la consulta del usuario
          response = {
            "msg" : "Añadir cuenta: error comprobando usuario.",
            "reserror" : true
          }
          res.status(500);
          res.send(response);
        }
        else
        {
          // Consulta del usuario ok
          if (body.length > 0)
          {
            // Usuario existe, se da de alta la cuenta
            // se calcula el numero de cuenta
            var numMili = Date.now();
            var cadMili = numMili.toString();
            var longCadMili = cadMili.length;
            var restoLongCadMili = 20 - longCadMili;
            var restoCadMili = "";
            for (var i=1;i<=restoLongCadMili;i++){
              restoCadMili = restoCadMili + "0";
            };
            var newIban = "ES01 " + restoCadMili + cadMili;
            var newAccount = {
                "user_id":  parseInt(idUser),
                "iban":  newIban,
                "balance":  0,
                "alias": alias
             };

            console.log("[addAccount] Se va a insertar la cuenta");
            console.log("[addAccount] user_id: " + idUser);
            console.log("[addAccount] iban: " + newIban);
            console.log("[addAccount] alias: " + alias);

            // se inserta la cuenta
            httpClient.post("account?" + mLabAPIKey ,newAccount,
              function(err,resMLabMov,bodyPost){
                if (err)
                {
                  // error en la insercion de cuenta
                  response = {
                    "msg" : "Añadir cuenta: error en inserción de cuenta.",
                    "reserror" : true
                  }
                  res.status(500);
                  res.send(response);
                }
                else
                {
                  // insercion de cuenta ok
                  response =  {
                    "msg" : "Añadir cuenta: cuenta añadida correctamente.",
                    "reserror" : false
                  };
                  console.log("[addAccount] Account correctly added");
                  res.send(response);
                }
              }
            );
          }
          else
          {
            // Usuario no existe, no se da de alta la cuenta
            response = {
              "msg" : "Añadir cuenta: usuario no encontrado. No se puede añadir cuenta.",
              "reserror" : true
            }
            res.status(404);
            res.send(response);
          }
        }
      }
    );
  } // function post addAccount
); // post addAccount




////////////////////////////////////////////////////////////////////////////////
//  Resto de métodos y funciones anteriores
////////////////////////////////////////////////////////////////////////////////
//
// gargallo&mozo 22/05/2018:
//
app.get("/apitechu/v1",
  function(req,res) {
    console.log("GET /apitechu/v1");
    //res.send("Respuesta desde API TechU");
    res.send ({"msg" : "Hola desde APITech"});
  }
);


//
// gargallo&mozo 22/05/2018:
//
  app.get("/apitechu/v1/users",
    function(req,res) {
      console.log("GET /apitechu/v1/users");
      res.sendFile('usuarios.json',{root: __dirname});
      /*var users = require ('./usuarios.json');
      res.send(users);*/
    }
  );


//
// gargallo&mozo 22/05/2018:
//
    app.post("/apitechu/v1/users",
      function (req,res){
        console.log ("POST  /apitechu/v1/users");
        console.log("first_name is " + req.body.first_name);
        console.log("last_name is " + req.body.last_name);
        console.log("country is " + req.body.country);
        var newUser ={
          "first_name": req.body.first_name,
          "last_name" : req.body.last_name,
          "country" : req.body.country
        };
        var users = require ('./usuarios.json');
        //users.push(req.body);
        users.push(newUser);
        writeUserDataToFile(users);

        console.log("Usuario guardado con exito ");
        res.send({"msg" : "Usuario guardado con exito"});

      }
  );


//
// gargallo&mozo 22/05/2018:
//
      app.delete("/apitechu/v1/users/:id",
        function (req,res){
          console.log ("DELETE  /apitechu/v1/users");
          console.log (req.params);
          console.log (req.params.id);
          var users = require ('./usuarios.json');
          users.splice(req.params.id-1,1);
          writeUserDataToFile(users);
          console.log ("usuario borrado");
            res.send({"msg" : "Usuario borrado con exito"});
      }
    );


//
// gargallo&mozo 22/05/2018:
//
    function writeUserDataToFile(data) {
      var fs = require ('fs');
      var jsonUserData = JSON.stringify(data,undefined,2);
      fs.writeFile("./login_usuarios.json",jsonUserData,"utf8",
      function (err){
        if (err) {
            console.log(err);
        } else {
            console.log("Datos escritos en archivo");
        }
      }
    );
    }


//
// gargallo&mozo 22/05/2018:
//
    app.post("/apitechu/v1/monstruo/:p1/:p2",
      function (req,res){
        console.log("Parametros ");
        console.log(req.params);
        console.log("Query String ");
        console.log(req.query);
        console.log("Body ");
        console.log(req.body);
        console.log("Headers ");
        console.log(req.headers);
      }
    );


//
// gargallo&mozo 22/05/2018:
//
    app.post("/apitechu/v1/login",
      function (req,res){
        console.log ("POST  /apitechu/v1/login");
        console.log("first_name is " + req.body.email);
        console.log("last_name is " + req.body.password);
        var newUser ={
          "email": req.body.email,
          "password" : req.body.password,
        };
        var users = require ('./login_usuarios.json');
        var salida={"mensaje" : "Login incorrecto" };
        for (user of users) {
          if  (user.email == newUser.email)
          {
            console.log("Usuario encontrado "  + user.id);
            if (user.password == newUser.password)
            {
              console.log("Password correcta");
              user.logged= true;
              //users.push(user);
              writeUserDataToFile(users);
              salida={"mensaje" : "Login correcto", "idUsuario" : user.id };
              break;
            }
            else{
              console.log("Password incorrecta");
              break;
            }

          }
          else {
              console.log("Usuario no encontrado "  + user.id);
          }

        }
        //users.push(req.body);


        res.send(salida);

      }
    );


//
// gargallo&mozo 22/05/2018:
//
    app.post("/apitechu/v0/logout",
      function (req,res){
        console.log ("POST  /apitechu/v2/logout");
        console.log("email is " + req.body.email);
        var query = 'q={"email":"' + req.body.email + '","logged":true}';
        console.log("Logout_Lanzo peticion " + "user?" + query  + "&" + mLabAPIKey  );
        var httpClient = requestJson.createClient(baseMLabURL);
        console.log("Logout_Cliente HTTP creado");
        httpClient.get ("user?" + query  + "&" + mLabAPIKey,
          function(err,resMLab,body){
            var response = {};
            if (err){
              response =  {"msg" : "Logout_Error obteniendo usuario " + req.body.email};
              res.status(500);
              res.send(response);
            } else {
              if (body.length > 0)
              {
                var putBody = '{"$unset":{"logged":""}}';
                httpClient.put ("user?" + query  + "&" + mLabAPIKey,JSON.parse(putBody),
                function(err2,resMLab2,body){
                  if (err){
                    response =  {"msg" : "Logout_Error al hacer el put" };
                    res.status(500);
                    res.send(response);
                  } else {
                      response =  {"msg" : "Logout_Usuario deslogado correctamente, OK al hacer el put"};
                      console.log("Logout_Ha hecho el PUT");
                  }
                  res.send(response);
                });
              } else {
                response = {"msg": "Logout_Usuario no encontrado"};
                res.status (404);
                res.send(response);
              }
            }
          });
    });


//
// gargallo&mozo 22/05/2018:
//
    app.post("/apitechu/v1/logout",
      function (req,res){
        console.log ("POST  /apitechu/v1/logut");
        console.log("id is " + req.body.id);
        var id =req.body.id;
        var users = require ('./login_usuarios.json');
        var salida={"mensaje" : "Logout incorrecto" };
        for (user of users) {
          if  (user.id == id)
          {
            console.log("Usuario encontrado "  + user.id);
            if (user.logged)
            {
              console.log("Usuario estaba logado");
              delete user.logged;
              //users.push(user);
              writeUserDataToFile(users);
              salida={"mensaje" : "Logout correcto", "idUsuario" : user.id };
              console.log("Usuario deslogado");
              break;
            }
            else{
              console.log("Usuario no estaba logado");
              break;
            }

          }
          else {
              console.log("Usuario no encontrado "  + user.id);
          }

        }
        //users.push(req.body);


        res.send(salida);

      }
    );


//
// gargallo&mozo 22/05/2018:
//
    app.post("/apitechu/v2/dummy",
      function (req,res){
        var response = {};
        console.log ("POST  /apitechu/v2/dummy");
        if (!auth.autentica(req,res)){
          console.log("GET /apitechu/v2/dummy token invalido");
          return res
          .status(401)
          .send({message: "Token invalido"});

        }
       else{
         console.log("GET /apitechu/v2/dummy token valido");

         response =  {"msg" : "Dummy OK",
                   "reserror" : false };
         console.log("dummy ok");
         res.send(response);
       }
     }
    );
