// auth.js
var moment = require('moment');
var jwt = require('jwt-simple');
var config = require('./config');

exports.autentica =  function (req,res){
  var response = {};
  console.log ("auth autentica");
  var token = req.headers.token;
  try {
   var payload = jwt.decode(token, config.TOKEN_SECRET);
   if(payload.exp <= moment().unix()) {
     console.log("El token ha expirado");
       return false;
   }
   else {
     console.log("El token es valido")
     return true;
   }
 } catch (e) {
   console.log("El token no es valido " + e);
   return false;
 }


}

exports.autenticaconNIF =  function (req,res){
  var response = {};
  console.log ("auth autentica");
  var token = req.headers.token;
  try {
    var payload = jwt.decode(token, config.TOKEN_SECRET);
    if(payload.exp <= moment().unix()) {
      console.log("El token ha expirado")
        // return res
        // .status(401)
        // .send({message: "El token ha expirado"});
        return false;
    }
    else if(payload.nif!=req.body.nif)
    {
      console.log("Se esta intentado modifica un NIF distinto al logado")
        // return res
        // .status(401)
        // .send({message: "Se esta intentado modifica un NIF distinso al logado"});
        return false;
    }
    else {
      console.log("El token es valido")
      return true;
    }
  } catch (e) {
    console.log("El token no es valido " + e);
    return false;
  }
}
