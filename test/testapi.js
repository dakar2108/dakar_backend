var mocha = require ('mocha');
var chai = require ('chai');
var chaihttp = require ('chai-http');

chai.use(chaihttp);

var should = chai.should();
//aranco el servidor
var server = require ('../server');
describe( 'First test suite',
  function(){
    it('Test that El Mundo works',
      function(done){
          chai.request ('http://www.elmundo.es')
            .get('/')
            .end(
              function (err,res){
                console.log("Request has ended");
                console.log(err);
                //console.log(res);
                res.should.have.status(200);
                done();
              }
            )
      }
    )
  }
);


describe( 'Test de API de usuarios Techu U',
  function(){
    it('Prueba que la API de usuarios Techu funciona correctamente ',
      function(done){
          chai.request ('http://localhost:3000')
            .get('/apitechu/v1')
            .end(
              function (err,res){
                console.log("Request has ended");
                //console.log(err);
                //console.log(res);
                res.should.have.status(200);
                res.body.msg.should.be.eql("Hola desde APITech");
                done();
              }
            )
      }
    ),
    it('Prueba que la API devuelve una lista de usuarios correctos ',
      function(done){
          chai.request ('http://localhost:3000')
            .get('/apitechu/v1/users')
            .end(
              function (err,res){
                console.log("Request has ended");
                //console.log(err);
                //console.log(res);
                res.should.have.status(200);
                res.body.should.be.a("array");
                for (user of res.body){
                  user.should.have.property('email');
                  user.should.have.property('password');
                  //user.should.have.property('password',"C6zUS8t6");
                }
                done();
              }
            )
      }
    )

  }
);
